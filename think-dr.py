# Parse correctly using Resource fork
import sys
import os
import getopt

import pagedirectory
import compressiontokens
import images
import xdef
import font
import html

import kaitaistruct
import page

from curses import wrapper

from io import BytesIO

compressionTokens = []
imageProvider = ''
currentDestinationFolder = ''
fontMapper = ''

inputFile = None


def printLog(s):
    with open('log.txt', 'a') as f2:
        f2.write('{}\n'.format(s))


def printError(s):
    with open('error.txt', 'a') as f2:
        f2.write('{}\n'.format(s))


def printStyle(s):
    with open('styles.txt', 'a') as f2:
        f2.write('{}\n'.format(s))


def generateIndexFile(inputFile, destinationFolder):
    global pages
    global fontMapper
    global imageProvider

    print('Generating index file at {}'.format(inputFile))
    outputPage = html.HTML(os.path.join(
        destinationFolder, 'index.html'), fontMapper, imageProvider)

    pages = xdef.XDef().get(inputFile)

    listOfLinks = []

    for pageIndex in range(0, len(pages)):
        title = pages[pageIndex]
        link = {'INDEX': pageIndex, 'TEXT': title}
        listOfLinks.append(link)

    outputPage.addListOfLinks(listOfLinks)

    outputPage.generateCSS()
    outputPage.generateHTML()


def pageToHTML(destinationFileName, articleNumber, p):
    global fontMapper
    global imageProvider

    pageTitle = p.title

    print('Page title {}'.format(pageTitle))

    outputPage = html.HTML(destinationFileName,
                           fontMapper, imageProvider, depth=1)

    lastStyle = None
    for chunkIndex in range(0, len(p.chunk)):
        chunk = p.chunk[chunkIndex]

        assets = readAssetsFromPascalString(chunk.content.text, lastStyle)

        for asset in assets:
            outputPage.addAsset(chunk.xcoord, chunk.ycoord, asset)

    outputPage.generateHTML()

    #printStyle('Article {} Styles Used {}'.format(articleNumber, stylesUsed))


def readAssetsFromPascalString(pascalString, lastStyle):
    # print('Reading complex string at offset 0x{:02X}'.format(fin.tell()))

    hadImage = False

    output = []

    global compressionTokens

    byteLen = len(pascalString)

    if byteLen == 0:
        return ''

    buffer = bytearray(pascalString, 'mac_roman')

    f = BytesIO(buffer)

    xOffsetInCharacters = 0

    bytearr = bytearray()

    bytesRead = 0
    currentStyle = 0

    if lastStyle is not None:
        currentStyle = lastStyle

    nextByteIsLiteral = False
    nextTokenModifier = None

    while (bytesRead < byteLen):
        b = f.read(1)
        bytesRead += 1

        if b >= b'\xf0' and b <= b'\xfe':
            nextTokenModifier = b
        elif b == b'\x00':
            # If we land on a null don't assume there is more to read
            if bytesRead == byteLen:
                break

            tokenId = int.from_bytes(f.read(1), 'big')
            bytesRead += 1

            # print('{} {}'.format(bytearr, tokenId))

            if nextTokenModifier == b'\xf0':  # Image
                # Store whatever we have so far as a string
                if len(bytearr) > 0:
                    output.append({'TEXT': bytearr.decode('mac_roman'),
                                   'STYLE': currentStyle})
                    bytearr = bytearray()

                width = int.from_bytes(f.read(2), 'big')
                bytesRead += 2
                output.append({'IMAGE': tokenId, 'WIDTH': width,
                              'XOFFSETCHARS': xOffsetInCharacters})
                hadImage = True
            elif tokenId < len(compressionTokens):
                tokenString = compressionTokens[tokenId]
                if nextTokenModifier == b'\xfb':
                    tokenString = tokenString.capitalize() + ' '
                elif nextTokenModifier == b'\xfc':
                    tokenString = tokenString.capitalize()
                elif nextTokenModifier == b'\xfd':
                    tokenString = tokenString + ' '
                elif nextTokenModifier == b'\xfe':
                    tokenString = ' ' + tokenString

                token = bytes(tokenString, 'mac_roman')
                bytearr += token
            else:
                printError('Couldnt interpret string value {}'.format(tokenId))
            # Reset the token modifier
            nextTokenModifier = None
        elif b >= b'\xc1' and b <= b'\xcf':
            if len(bytearr) > 0:
                output.append({'TEXT': bytearr.decode('mac_roman'),
                               'STYLE': currentStyle})
            bytearr = bytearray()
            currentStyle = int.from_bytes(b, 'big') - 193
        elif b == b'\xff':
            nextByteIsLiteral = True
        elif b >= b'\x80' and b <= b'\xd8' and not nextByteIsLiteral:
            spaceOffsetPixels = int.from_bytes(b, 'big')
            spaces = int(spaceOffsetPixels / 10)
            xOffsetInCharacters += spaces
            for spaceIndex in range(spaces):
                bytearr += '&nbsp;'.encode('mac_roman')
        else:
            bytearr += b
            xOffsetInCharacters += 1
            nextByteIsLiteral = False

    if len(bytearr) > 0:
        output.append({'TEXT': bytearr.decode('mac_roman'),
                       'STYLE': currentStyle})

    if hadImage:
        with open("images.txt", "a") as file:
            file.write('Image found in {}\n'.format(buffer.hex()))
        # print('{} : {}'.format(bytearr, bytearr.decode('mac_roman')))
    return output


def readSimpleString(data):
    byteLen = int.from_bytes(data.read(2), 'big')
    s = bytearray()

    for i in range(0, byteLen):
        s += data.read(1)

    # Strings are padded
    if byteLen % 2 is not 0:
        data.read(1)

    return s.decode('mac_roman')


def readPage(articleNumber, inputFileName, data):
    bytesIO = kaitaistruct.BytesIO(data)

    print('Reading page from article number {}'.format(articleNumber))

    p = page.Page(kaitaistruct.KaitaiStream(bytesIO))

    zinename = os.path.basename(inputFileName)
    destinationFileName = os.path.join(
        zinename, '{}'.format(articleNumber), 'index.html')
    # print('folder {}'.format(folder))

    # print('offset {}'.format(data.tell()))
    pageToHTML(destinationFileName, articleNumber, p)

    # printLog('Finished reading article {} titled {} at offset {}'.format(
    #    articleNumber, pageTitle, bytesIO.tell()))


def extractPage(index, inputFile, pageOffset, pageEnd):
    print('Page Offset {:02X}'.format(pageOffset))
    file = open(inputFile, 'rb')
    file.seek(pageOffset)

    data = file.read(pageEnd - pageOffset)

    file.close()

    readPage(index, inputFile, data)

    printLog('Page contains {} bytes'.format(len(data)))
    # sys.exit(0)


def readResourceFork():
    global pageDir
    pageDir = pagedirectory.PageDirectory()
    global compressionTokens
    compressionTokens = compressiontokens.CompressionTokens().get(inputFile)
    global imageProvider
    imageProvider = images.ImageProvider(inputFile)
    global fontMapper
    fontMapper = font.FontMapper(inputFile)


def extractFile():

   # stdscr.clear()
    readResourceFork()

    global inputFile

    # stdscr.addstr(1, 1, 'Generating index file...')

    destinationFolder = os.path.basename(inputFile)

    generateIndexFile(inputFile, destinationFolder)

    # stdscr.refresh()
    global pageDir
    pages = pageDir.get(inputFile)

    finalPageOffset = pages[len(pages) - 1]
    fileSize = os.stat(inputFile).st_size

    # When the final PgDr marker is the file size (like with Develop articles) the index page appears to be first in the file, otherwise its the last page
    indexPageIndex = len(pages) - 1
    if finalPageOffset == fileSize:
        indexPageIndex = 0

    for i in range(0, len(pages)):
        pageStart = pages[i]

        pageEnd = os.stat(inputFile).st_size

        if i < len(pages) - 1:
            pageEnd = pages[i + 1]

        # try:
         #       stdscr.addstr(
         #           3, 1, 'Extracting page {} of {}...'.format(i, len(pages)))
         #       stdscr.refresh()
            extractPage(i, inputFile, pageStart, pageEnd)
        # except Exception as e:
         #   print(
        #        'Catastrophic Failure to parse page {} : {}'.format(i, e))


def usage():
    print("think-dr.py -i <inputfile> -o <outputfile>")


def main(argv):
    global inputFile
    print("Think De Reverent")
    print("=================")

    try:
        opts, args = getopt.getopt(argv, "hi:o", ["ifile=", "ofile="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    inputFile = None

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputFile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    if inputFile is None:
        print("Input file must be specified")
        sys.exit(2)

    print("Input file : {}".format(inputFile))

    os.system('rm error.txt')
    os.system('rm log.txt')
    os.system('rm styles.txt')

    extractFile()


if __name__ == "__main__":
    main(sys.argv[1:])
