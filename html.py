import domonic
import os


class HTML:

    def __init__(self, destinationFileName, fontMapper, imageProvider, depth=0):
        self.destinationFileName = destinationFileName
        self.fontMapper = fontMapper
        self.imageProvider = imageProvider
        self.site = domonic.html()
        self.lastYCoord = 0
        self.destinationFolder = os.path.dirname(destinationFileName)

        cssPath = 'default.css'
        for i in range(0, depth):
            cssPath = '../' + cssPath

        dbStyle = domonic.link(_href=cssPath,
                               _rel="stylesheet")

        head = self.site.createElement('head')
        head.append(domonic.meta(_content='text/html; charset=utf-8'))
        head.append(dbStyle)
        self.site.append(head)

        self.container = self.site.createElement('div')
        self.container.style.backgroundColor = 'white'
        self.container.style.width = '450px'
        self.container.style.margin = 'auto'
        self.container.style.position = 'relative'

        self.site.append(self.container)
        self.site.style.backgroundColor = '#777777'

    def generateCSS(self):
        zinename = os.path.dirname(self.destinationFileName)
        cssFilename = os.path.join(zinename, 'default.css')
        os.makedirs(zinename, exist_ok=True)

        with open(cssFilename, 'w') as f:
            for id in range(0, self.fontMapper.count()):
                style = self.fontMapper.get(id)
                f.write('.style{} {{\n'.format(id))
                f.write('\tfont-family: {};\n'.format(style['FONT']))
                f.write('\tfont-size: {};\n'.format(style['SIZE']))

                if 'BOLD' in style:
                    f.write('\tfont-weight: bold;\n')

                if 'ITALIC' in style:
                    f.write('\tfont-style: italic;\n')

                f.write('}\n')

    def getTextElementFromAsset(self, asset):
        global site
        global fontMapper
        text = self.site.createTextNode(asset['TEXT'])
        styleIndex = asset['STYLE']

        span = domonic.span(_class="style{}".format(styleIndex + 1))
        span.append(text)

        return span

    def getImageElementFromAsset(self, lastYCoord, xcoord, ycoord, asset):
        imageIndex = asset['IMAGE']
        width = asset['WIDTH']
        xOffsetInCharacters = asset['XOFFSETCHARS']

        im = self.imageProvider.generateAndReturnPath(
            self.destinationFolder, imageIndex)

        if im is None:
            div = self.site.createElement('div')
            div.append(self.site.createTextNode('Missing image Resource!'))
            return div

        imageNode = domonic.img(_src=im['path'], _width='{}'.format(width))

        y = ycoord - im['height']

        # Nasty Hacks
        if y < 0:
            y = 0
        if y < self.lastYCoord + 11:
            y = self.lastYCoord + 11

        div = self.site.createElement('div')
        div.style.position = 'absolute'
        div.style.left = '{}px'.format(xcoord + (9 * xOffsetInCharacters))
        div.style.top = '{}px'.format(y)
        div.append(imageNode)

        return div

    def addListOfLinks(self, listOfLinks):
        ul = domonic.ul()
        ul.style.fontFamily = "'Arial'"

        for index in range(0, len(listOfLinks)):
            link = listOfLinks[index]

            li = domonic.li()
            path = '{}/index.html'.format(link['INDEX'])
            container = domonic.a(_href=path)
            text = self.site.createTextNode(link['TEXT'])
            container.append(text)
            li.append(container)
            ul.append(li)

        self.site.append(ul)

    def addAsset(self, x, y, asset):
        if 'TEXT' in asset:
            style = asset['STYLE']

            paragraph = self.site.createElement('div')
            paragraph.style.position = 'absolute'
            paragraph.style.padding = '0'
            paragraph.style.margin = '0'
            paragraph.style.left = '{}px'.format(x)
            paragraph.style.top = '{}px'.format(y)
            paragraph.append(self.getTextElementFromAsset(asset))
            self.container.append(paragraph)

            lastStyle = style
        if 'IMAGE' in asset:
            self.container.append(self.getImageElementFromAsset(
                self.lastYCoord, x, y, asset))

        self.lastYCoord = y

    def generateHTML(self):
        if self.container:
            self.container.style.height = '{}px'.format(self.lastYCoord)

        os.makedirs(os.path.dirname(self.destinationFileName), exist_ok=True)
        print(self.destinationFileName)
        with open(self.destinationFileName, 'w') as f2:
            f2.write(self.site.toString())
