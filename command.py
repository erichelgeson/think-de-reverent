import subprocess


class Command:

    def __init__(self, command):
        self.command = command

    def run(self):
        proc = subprocess.Popen(
            [self.command], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
