import rez
import os
import shutil
import command
from PIL import Image


class ImageProvider:

    def __init__(self, fileName):
        self.fileName = fileName

    def generateAndReturnPath(self, destinationFolder, index):
        data = rez.getIndividualData(self.fileName, 'PICT', index)
        if data is None:
            return None

        shutil.rmtree('tmp', ignore_errors=True)
        os.makedirs('tmp', exist_ok=True)

        with open(os.path.join('tmp', 'temp_pict.PICT'), 'w+b') as f:
            f.write(data)

        command.Command(
            'deark {} -od {}'.format(os.path.join('tmp', 'temp_pict.PICT'), 'tmp')).run()
        #print(os.listdir(os.path.join('.', 'tmp')))

        try:
            numberOfStrips = len([name for name in os.listdir(os.path.join('.', 'tmp')) if os.path.isfile(os.path.join('.', 'tmp',
                                                                                                                       name)) and name.startswith('output.')])
            #print('number of strips {}'.format(numberOfStrips))
            if numberOfStrips > 1:
                os.system(
                    'convert -append tmp/*.png tmp/{}.png'.format(index))
            else:
                os.system('mv tmp/output.000.png tmp/{}.png'.format(index))

            im = Image.open("tmp/{}.png".format(index))
            width = im.size[0]
            height = im.size[1]
            im.close()

            os.system('mv tmp/{}.png "{}"'.format(index,
                                                  os.path.join(destinationFolder, '{}.png'.format(index))))
        except:
            return None

        shutil.rmtree('tmp', ignore_errors=True)

        return {'path': '{}.png'.format(index), 'width': width, 'height': height}
