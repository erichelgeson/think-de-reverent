import rez

import sys
import os
import getopt

from io import BytesIO

# Stores the names of the pages


class XDef:

    def get(self, fileName):
        pages = {}

        for id in range(128, 255):
            data = rez.getIndividualData(fileName, 'xdef', id)
            if data is None:
                break

            f = BytesIO(data)
            unknown = int.from_bytes(f.read(2), 'big')
            while f.tell() < len(data):
                pageNumber = int.from_bytes(f.read(2), 'big')
                pageNameLengthBytes = int.from_bytes(f.read(1), 'big')
                pageName = f.read(pageNameLengthBytes).decode('mac_roman')
                # print('addr {:2X} page num {} page bytes {}'.format(
                #   f.tell(), pageNumber, pageNameLengthBytes))
                if pageNameLengthBytes % 2 == 0:
                    f.read(1)
                pages[pageNumber] = pageName

        return pages
