import sys
import os
import getopt
import shutil
import rsrcfork


def getIndividualData(fileName, rType, index):
    rf = rsrcfork.open(fileName)
    resType = bytes(rType, 'mac_roman')
    resources = rf[resType]

    if not index in resources:
        return None

    resource = resources[index]
    return resource.data
